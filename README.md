# Profile of Frank de Bot

## Personalia
%PLACEHOLDER_PRESONALIA%

## Skills
ansible, apache, centos, ceph, ci/cd, debian, devops, ethical hacking, 
freebsd, high availability, linux, linux kvm, soaring, lpic-3, nginx, php, redhat certified engineer, zfs, 
jenkins, scrum, agile, vmware, infra, itil, testing, running, networking, cisco, nsx, python, 
postgresql, gardening, mariadb, perl, storage, bash, security, git, iot, nodemcu, adruino, terraform,
redis,

## Certificates
* EX125 Red Hat Certified Specialist in Ceph Storage Administration 
* Red Hat Certified Specialist in Security 
* AWS Certified Solutions Architest - Associate 
* Expertise in Ansible Automation (EX407) 
* RedHat RHCE 
* LPIC-3 - Virtualization and High Availability 
* Professional Scrum Master 
* ITIL Foundation 
* ISTQB - International Software Testing  
* Coming up certificates: OpenShift, OpenStack

The certificates are a part of a life long learning. By personal interest new techniques are made my own.

## Work experience
### KPN B.V. ( 2013 - now )
#### Infrastructure DevOps Specialist. ( 2018 - now )
Notable work:

* Creating infrastructure-as-code with cloudformation, terraform and ansible
* Working in small team, building and maintaining a vmware based private cloud.
* Automation of rollout of PostgreSQL platform via terraform and ansible.
* Maintaining mailplatform of Telfort.
* Migrating away of mailplatform of Telfort.
* Supporting infrastructure of the Telfort CRM and ticketing system

#### Application Engineer Cinco CRM ( 2013 - 2018 )
Notable work: 

* Developing CRM and deliverystreet for Telfort residenatial customers
* Creating backend and API for MijnTelfort frontend.
* Rebuilding deliverystreet to leverage message queue and worker functionality
* Implementing external API for services like DSL, Voip, Invoicing, Debt Management
* 3th line support

### Skool ( 2012 - 2013 )
#### Application Engineer
Notable work: 

* Creating backend API's for webbased frontend management system for rolling out PC's for children in elementairy schools

### NCA B.V. ( 2006 - 2012 )
#### ICT Consultant / System engineer / Software Engineer
Notable work:

* Design and implementing office network based on Linux en Windows, networking with Cisco and Astaro
* 1st line support office employees
* Creating application for hour registration, registering subcontractor receipts, central project register

### Ematic Interactive ( 2005 - 2006 )
#### Software Engineer
Notable work:

* Developing and supporting an online fitness application. Creating algorithm for personal scheme's, integrating payment options 
