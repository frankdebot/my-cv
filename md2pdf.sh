#!/bin/bash

gpg2 -d personalia.md.gpg > /tmp/personalia.md || exit 1

DATE=$(date +%c)

perl -e '$date=time(); open(A,"README.md");open(B,"personalia.md");$b = join("",<B>);close(B);while(<A>){ $_ =~ s/%PLACEHOLDER_PRESONALIA%/$b/g; $_ =~ s/%PLACEHOLDER_DATETIME%/$date/g; print $_} close(A)' > /tmp/mycv_personalia.md # welll, couldn't figure it out properly with sed, it messed up newlines :-p
perl -e '$date=time(); open(A,"README.md");while(<A>){ $_ =~ s/%PLACEHOLDER_PRESONALIA%/hidden/g; $_ =~ s/%PLACEHOLDER_DATETIME%/$date/g; print $_} close(A)' > /tmp/mycv.md # welll, couldn't figure it out properly with sed, it messed up newlines :-p

pandoc -V geometry:margin=1in -V papersize:a4 -V fontfamily:dejavu  --from markdown -o profile.pdf /tmp/mycv.md
pandoc -V geometry:margin=1in -V papersize:a4 -V fontfamily:dejavu  --from markdown -o profile_withpersonalia.pdf /tmp/mycv_personalia.md


# Clean
#rm /tmp/personalia.md
#rm /tmp/mycv.md